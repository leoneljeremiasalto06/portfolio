
var darkmode;
var menuchico = false;
var menudesplegado = false;
var menugrande = true;
var menuActivado = false;

document.addEventListener("DOMContentLoaded", function () {
  iniciarApp();
  calcularMenu();
  verificarTema();
  scrollNav();
});

function verificarTema(){
  var tema = window.matchMedia('(prefers-color-scheme: dark)').matches;
  if (tema) {
    botonDarkMode.classList.add("darkmode");
    botonDarkMode.textContent = "brightness_7";
    darkmode = true;
    crearDarkMode();
  }
  else {
    eliminarDarkMode();
    darkmode = false;
  }
}

function cambiarTema(){
  if (darkmode) {
    botonDarkMode.classList.remove("darkmode");
    botonDarkMode.textContent = "dark_mode";
    eliminarDarkMode();
    darkmode = false;
  }
  else {
    botonDarkMode.classList.add("darkmode");
    botonDarkMode.textContent = "brightness_7";
    crearDarkMode();
    darkmode = true;
  }
}
//boton
var menu = document.getElementById("navbar");
botonDarkMode = document.getElementById("darkmode");
botonDarkMode.addEventListener("click", function () {
  cambiarTema();
});

window.addEventListener('resize', function () {
  calcularMenu();
  
});


function crearDarkMode() {
  //body
  var body = document.querySelector("body");
  body.classList.add("darkmode");

  //barra de navegación
  var menu = document.getElementById("navbar");
  menu.classList.add("darkmode");
  var contenidoDelMenu = document.querySelector(".menu");
  contenidoDelMenu.classList.add("darkmode");


  if (document.querySelector(".menu-chico") !== null) {
    var menuChico = document.querySelector(".menu-chico");
    menuChico.classList.add("darkmode");
  }
  //enlaces
  var enlaces = document.querySelectorAll("li a");

  enlaces.forEach(enlace => {
    enlace.classList.add("darkmode");
  });

  var h2 = document.querySelectorAll("h2");
  h2.forEach(h2 => {
    h2.classList.add("darkmode__texto");
  })
  var legends = document.querySelectorAll("legend");
  legends.forEach(legend => {
    legend.classList.add("darkmode__texto")
  })
  var labels = document.querySelectorAll("label");
  labels.forEach(label => {
    label.classList.add("darkmode__texto")
  })

  var copy = document.getElementById("copy");
  copy.classList.add("darkmode__texto");
}

function eliminarDarkMode() {

  //body
  var body = document.querySelector("body");
  body.classList.remove("darkmode");

  //barra de navegación
  var menu = document.getElementById("navbar");
  menu.classList.remove("darkmode");
  var contenidoDelMenu = document.querySelector(".menu");
  contenidoDelMenu.classList.remove("darkmode");

  if (document.querySelector(".menu-chico") !== null) {
    var menuChico = document.querySelector(".menu-chico");
    menuChico.classList.remove("darkmode");
  }
  //enlaces
  var enlaces = document.querySelectorAll("li a");
  enlaces.forEach(enlace => {
    enlace.classList.remove("darkmode");
  });


  var h2 = document.querySelectorAll("h2");
  h2.forEach(h2 => {
    h2.classList.remove("darkmode__texto");
  })

  var legends = document.querySelectorAll("legend");
  legends.forEach(legend => {
    legend.classList.remove("darkmode__texto")
  })
  var labels = document.querySelectorAll("label");
  labels.forEach(label => {
    label.classList.remove("darkmode__texto")
  })

  var copy = document.getElementById("copy");
  copy.classList.remove("darkmode__texto");

}


function scrollNav() {
  const enlaces = document.querySelectorAll('.menu li a');
  enlaces.forEach(enlace => {
    enlace.addEventListener('click', function (e) {
      e.preventDefault();
      const seccionScroll = e.target.attributes.href.value;
      const seccion = document.querySelector(seccionScroll);
      seccion.scrollIntoView({ behavior: "smooth" })
    })
  })

}


function iniciarApp() {
  // Desplazamiento hacia abajo de la barra de navegación
  setTimeout(function () {
    document.getElementById("navbar").classList.add("show");
  }, 500); // Se inicia después de 500ms (puedes ajustar este valor según sea necesario)

}


//menu
function calcularMenu() {
  var windowWidth = window.innerWidth;
  enlaces = document.querySelectorAll('.menu li');
if(windowWidth >= 768) {
  menudesplegado = true;
  enlaces.forEach(enlace => {
    enlaceEstilo = enlace.style;
    enlaceEstilo.display = "flex";
    enlaceEstilo.margin = "0 auto"; // Centra verticalmente el contenido del enlace
  }
  );
}
  if (windowWidth < 768) {
    if (menuchico === false && menugrande === true) {

      // Crear un elemento <a>
      var enlace = document.createElement("a");

      // Agregar la clase "menu-chico" al elemento <a>
      enlace.classList.add("menu-chico");

      // Ahora puedes agregar más atributos al enlace si es necesario
      enlace.href = "#"; // Agregar un enlace

      // Agregar texto dentro del enlace
      enlace.textContent = "Menu";

      var menu = document.querySelector(".menu")
      menu.appendChild(enlace);
      menugrande = false;

      var enlace = document.querySelector(".menu .menu-chico");
      enlace.addEventListener("click", function (e) {
        e.preventDefault();
        if(menudesplegado === false){
        enlaces = document.querySelectorAll('.menu li');
        enlaces.forEach(enlace => {
          enlaceEstilo = enlace.style;
          enlaceEstilo.display = "flex";
          enlaceEstilo.margin = "0 auto"; // Centra verticalmente el contenido del enlace
        }
        );
        menudesplegado = true;
      }
      else{
        if(windowWidth < 768){
          enlaces = document.querySelectorAll('.menu li');
          enlaces.forEach(enlace => {
            console.log(enlace);
          enlaceEstilo = enlace.style;
          enlaceEstilo.display = "none";
        });
        menudesplegado = false;
      }
      }
      })
    }

    if (menuchico === true && menugrande === false) {
      menuchico = true;
      menugrande = false;

    }
  }
  if(darkmode){
    crearDarkMode();
  }
}



//botones:
document.addEventListener('DOMContentLoaded', () => {
  // Selecciona el icono con id 'html'
  const iconHtml5 = document.getElementById('html');

  // Añade un listener para el evento 'mouseover'
  iconHtml5.addEventListener('mouseover', function () {
      // Verifica si el <span> con la clase 'textHTML5' ya existe dentro del icono
      const existingSpan = iconHtml5.querySelector('.textHTML5');

      if (!existingSpan) {
          // Crea un nuevo elemento <span>
          const spanText = document.createElement('span');

          // Establece la clase del <span>
          spanText.className = 'textHTML5';

          // Puedes establecer el texto o contenido del <span>
          spanText.innerHTML = 'HTML5 define la estructura de la página web y el contenido, como texto, imágenes y enlaces.';
          // Agrega el <span> al icono
          iconHtml5.appendChild(spanText);

          // Usar requestAnimationFrame para forzar el reflow antes de añadir la clase visible
          requestAnimationFrame(() => {
              spanText.classList.add('visible');
          });
      }
  });

  // Añade un listener para el evento 'mouseout'
  iconHtml5.addEventListener('mouseout', function () {
      const existingSpan = iconHtml5.querySelector('.textHTML5');

      if (existingSpan) {
          // Primero quita la clase visible para iniciar la transición de salida
          existingSpan.classList.remove('visible');

          // Usa un temporizador para esperar el tiempo de la transición antes de eliminar el elemento
          setTimeout(() => {
              iconHtml5.removeChild(existingSpan);
          }, 300); // Debe coincidir con la duración de la transición en CSS
      }
  });
});




document.addEventListener('DOMContentLoaded', () => {
  const iconCSS = document.getElementById('css');

  iconCSS.addEventListener('mouseover', function () {
      const existingSpan = iconCSS.querySelector('.textCSS');

      if (!existingSpan) {
          const spanText = document.createElement('span');

          spanText.className = 'textCSS';

          spanText.innerHTML = 'CSS3 define cómo se deben mostrar los elementos en la pagina web sus caracteristicas, como colores, fuentes, espaciados y diseño.';

          iconCSS.appendChild(spanText);

          requestAnimationFrame(() => {
              spanText.classList.add('visible');
          });
      }
  });

  iconCSS.addEventListener('mouseout', function () {
      const existingSpan = iconCSS.querySelector('.textHTML5');

      if (existingSpan) {
          existingSpan.classList.remove('visible');

          setTimeout(() => {
            iconCSS.removeChild(existingSpan);
          }, 300); 
      }
  });
});




document.addEventListener('DOMContentLoaded', () => {
  const iconSASS = document.getElementById('sass');

  iconSASS.addEventListener('mouseover', function () {
      const existingSpan = iconSASS.querySelector('.textSASS');

      if (!existingSpan) {
          const spanText = document.createElement('span');

          spanText.className = 'textSASS';

          spanText.innerHTML = 'Sass facilita la escritura y el mantenimiento de hojas de estilo complejas, haciéndolas más organizadas y reutilizables.';

          iconSASS.appendChild(spanText);

          requestAnimationFrame(() => {
              spanText.classList.add('visible');
          });
      }
  });

  iconSASS.addEventListener('mouseout', function () {
      const existingSpan = iconSASS.querySelector('.textHTML5');

      if (existingSpan) {
          existingSpan.classList.remove('visible');

          setTimeout(() => {
            iconSASS.removeChild(existingSpan);
          }, 300); 
      }
  });
});



document.addEventListener('DOMContentLoaded', () => {
  const iconJS = document.getElementById('js');

  iconJS.addEventListener('mouseover', function () {
      const existingSpan = iconJS.querySelector('.textJS');

      if (!existingSpan) {
          const spanText = document.createElement('span');

          spanText.className = 'textJS';

          spanText.innerHTML = 'Se utilizo Javascript principalmente para crear y controlar contenido dinámico en páginas web.';

          iconJS.appendChild(spanText);

          requestAnimationFrame(() => {
              spanText.classList.add('visible');
          });
      }
  });

  iconJS.addEventListener('mouseout', function () {
      const existingSpan = iconJS.querySelector('.textHTML5');

      if (existingSpan) {
          existingSpan.classList.remove('visible');

          setTimeout(() => {
            iconJS.removeChild(existingSpan);
          }, 300); 
      }
  });
});

document.addEventListener('DOMContentLoaded', () => {
  const iconPHP = document.getElementById('php');

  iconPHP.addEventListener('mouseover', function () {
      const existingSpan = iconPHP.querySelector('.textPHP');

      if (!existingSpan) {
          const spanText = document.createElement('span');

          spanText.className = 'textPHP';

          spanText.innerHTML = 'Se utilizó PHP en este proyecto para manejar la lógica del servidor, procesar datos de formularios e interactuar con la base de datos.';

          iconPHP.appendChild(spanText);

          requestAnimationFrame(() => {
              spanText.classList.add('visible');
          });
      }
  });

  iconPHP.addEventListener('mouseout', function () {
      const existingSpan = iconPHP.querySelector('.textPHP');

      if (existingSpan) {
          existingSpan.classList.remove('visible');

          setTimeout(() => {
            iconPHP.removeChild(existingSpan);
          }, 300); 
      }
  });
});







document.addEventListener('DOMContentLoaded', () => {
  const iconSQL = document.getElementById('sql');

  iconSQL.addEventListener('mouseover', function () {
      const existingSpan = iconSQL.querySelector('.textSQL');

      if (!existingSpan) {
          const spanText = document.createElement('span');

          spanText.className = 'textSQL';

          spanText.innerHTML = 'En el proyecto, se utilizo SQL para gestionar y manipular datos dentro de la base de datos administrada por MySQL.';

          iconSQL.appendChild(spanText);

          requestAnimationFrame(() => {
              spanText.classList.add('visible');
          });
      }
  });

  iconSQL.addEventListener('mouseout', function () {
      const existingSpan = iconSQL.querySelector('.textSQL');

      if (existingSpan) {
          existingSpan.classList.remove('visible');

          setTimeout(() => {
            iconSQL.removeChild(existingSpan);
          }, 300); 
      }
  });
});
