//Extraccion de dependencias para leer, escribir y mirar los cambios
const { src, dest, watch, parallel } = require('gulp');
// todas las funcionalidades de gulp
//en particular selecciona la funcionalidad src y dest de gulp y lo trae a gulpfile.js
//pipe es una funcon que permite ejecutar una accion despues de otra o una funcionalidad;
const sass = require('gulp-sass')(require('sass'));
var plumber = require('gulp-plumber');
const cache = require('gulp-imagemin');


//Extraccion de dependencias para las imagenes
const webp = import('gulp-webp');
const imagemin = require('gulp-cache');
const avif = require('gulp-avif');

function css(done) {
    src('src/scss/**/*.scss') // Identificar el archivo .SCSS a compilar
        .pipe(plumber())
        .pipe(sass()) // Compilarlo
        .pipe(dest('build/css')) // Almacenarla en el disco duro
    done();
}

function dev(done) {
    watch('src/scss/**/*.scss', css); /*estara mirando la funcion watch ¿que archivo estare 
    escuchando? ¿Que funcion ejecutare cuando se produzca un cambio en el archivo? */
    watch('src/js/**/*.js', js);
    done();
}

function js(done){
    src('src/**/*.js')
    .pipe(dest('build/'));
    done();
}
async function versionWebp(done) {
    const opciones = {
        quality: 50
    };
    //Aquí está la clave para que funcione y evitar los problemas que ya había 
    const webpModule = await webp;
    src('src/scss/img/**/*.{png,jpg}')
        .pipe(webpModule.default(opciones))
        .pipe(dest('build/img'));
    done();
 }
async function imagenes(callback) {
    const opciones = {
        optimizationLevel: 3
    }
    src('src/scss/img/**/*.{png,jpg}')
        .pipe(cache(imagemin(opciones)))
        .pipe(dest('build/img'))
    callback();
}

async function versionAvif(callback) {
    const opciones = {
        quality: 50
    };
    //Aquí está la clave para que funcione y evitar los problemas que ya había 
    src('src/scss/img/**/*.{png,jpg}')
        .pipe(avif(opciones))
        .pipe(dest('build/img'));
    callback();
}


exports.versionWebp = versionWebp;
exports.versionAvif = versionAvif;
exports.dev = dev;
exports.css = css;
exports.js = js;
exports.run = parallel(imagenes, versionWebp, versionAvif, dev, js);
